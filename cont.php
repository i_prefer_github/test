<?php
    
    function display(){
      $dis = New Display();
      $dis->getData();
      $dis->con->close();
    }

    function onrequest() {
      if(isset($_POST['Delete'])) {
        $del = New Delete();
        $del->delData();
        $del->con->close();
      }
      if(isset($_POST['Save1'])) {
        $del = New Add();
        $del->addData();
        $del->con->close();
      }
      echo '<script>window.location="https://shiftless-sports.000webhostapp.com"</script>';
    }

    class Controller{
        protected $host = 'localhost';
        protected $user = 'id18756876_me';
        protected $pass ='qH^4MV#0]eYO@%)D';
        protected $dtBase = 'id18756876_items_i';

        public $con = null;

        public function __construct(){
            $this->con = mysqli_connect($this->host, $this->user, $this->pass, $this->dtBase);
            if($this->con->connect_error){
                echo "Fail". $this->con->connect_error;
            }
        }
    }

     Class Delete extends Controller{
      public function delData(){
        if(isset($_POST["Delete"]))
        {
          $box = $_POST['delete-checkbox'];

          foreach($box as $key => $val)
          {
            $bal = explode("_", $val);

            mysqli_query($this->con,"DELETE product, product_attribute, attribute 
                          FROM product, product_attribute, attribute 
                          where product.SKU = '$bal[0]'
                          AND product_attribute.SKU = '$bal[0]'
                          AND product_attribute.attributeid = '$bal[1]'
                          AND attribute.attributeid = '$bal[1]'; ");
          }
        }
        
      }
    }

    class Display extends Controller{

      public function getData(){
        $prod1 = "SELECT *
                    from product p
                    left join product_attribute t
                    on p.SKU = t.SKU
                    left join attribute a
                    on t.attributeid = a.attributeid";
        $prod_result = $this->con->query($prod1);

        if ($prod_result->num_rows > 0) {

            while($row = $prod_result->fetch_assoc()) {
              echo "<label class=\"lbl-checkbox\"><div class=\"grid-item\"><input class=\"delete-checkbox\" name = \"delete-checkbox[]\"  type=\"checkbox\" value=".$row["SKU"]."_".$row["attributeid"]."> <p>SKU: " . $row["SKU"]. "</p> <p>Name: " . $row["name"]. "</p> <p>Price: " . $row["price"]."$</p>  <p>Info: " . $row["attribute_value"]."</p></input></div></label>";
            }
          } else {
            echo "<label class=\"lbl-checkbox\"><div class=\"grid-item\">0 results</div></label>";
          } 
      }
    }

    class Add extends Controller{
      
      public function addData(){
        $SKU =  $_REQUEST['sku'];
        $name = $_REQUEST['name'];
        $Price =  $_REQUEST['price'];
        $productType = $_REQUEST['productType'];
        $atr = array();
        $natr = null;
           switch ($productType) {
              case 'DVD':
                array_push($atr,$_REQUEST['size']);
                $natr = "MB";
              break;
              case 'Furniture':
                array_push($atr,$_REQUEST['height'],"x",$_REQUEST['width'],"x",$_REQUEST['length']);
                $com = implode($atr);
                $natr = "HxWxL";
              break;
              case 'Book':
                array_push($atr,$_REQUEST['weight']);
                $natr = "KG";
              break;
            default: break;
          }
        
        $com = implode($atr);
        $r =$SKU . $com . $natr;
        $enc = new Encrypt();

        $er = $enc->Encipher($r,5);

      
        $sql = "INSERT INTO attribute VALUES ('$er','$natr', '$com$natr');";
        $sql .= "INSERT INTO product_attribute VALUES ('$SKU','$er');";
        $sql .= "INSERT INTO product VALUES ('$SKU', '$name', '$Price')";
                
        
        if ($this->con->multi_query($sql) === TRUE) {
          echo "New records created successfully";
        } else {
          echo "Error: " . $sql . "<br>" . $conn->error;
        }

      }
    }

    class Encrypt{
      public function Cipher($ch, $key){
        if (!ctype_alpha($ch))
          return $ch;

        $offset = ord(ctype_upper($ch) ? 'A' : 'a');
        return chr(fmod(((ord($ch) + $key) - $offset), 26) + $offset);
      }

      public function Encipher($input, $key){
        $output = "";

        $inputArr = str_split($input);
        foreach ($inputArr as $ch)
          $output .=$this->Cipher($ch, $key);

        return $output;
      }

      public function Decipher($input, $key){
          return $this->Encipher($input, 26 - $key);
      }
    }

?>